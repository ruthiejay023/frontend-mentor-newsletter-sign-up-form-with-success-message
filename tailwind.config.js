/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./public/**/*.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {},
    colors: {
      'tomato': 'hsl(4, 100%, 67%)',
      'dark-slate-grey': 'hsl(234, 29%, 20%)',
      'chacoal-grey': 'hsl(235, 18%, 26%)',
      'grey': 'hsl(231, 7%, 60%)',
      'white': 'hsl(0, 0%, 100%)',
      'light-grey': 'hsla(243, 28%, 13%, 0.25)',
      'error': 'hsla(4, 100%, 67%, 0.15)',
      'dark': '#eaeaea'
    },
    fontFamily: {
      Roboto: ['Roboto', 'cursive']
    }
  },
  plugins: [],
}

